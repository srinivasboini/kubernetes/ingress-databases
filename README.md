# ingress-databases

## Add bitnami repo 

```
helm repo add bitnami https://charts.bitnami.com/bitnami  

```

## mysql template using helm

```
helm search repo bitnami/mysql --versions

helm template mysql bitnami/mysql --version=9.10.1 > mysql-9.10.1.yaml

helm show values bitnami/mysql > values.yaml

```

